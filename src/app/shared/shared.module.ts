import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule }   from '@angular/router';

import { NavigasiBarComponent } from './navigasi-bar/navigasi-bar.component';

@NgModule({
  imports: [
    CommonModule, RouterModule
  ],
  declarations: [NavigasiBarComponent],
  exports: [
        NavigasiBarComponent
  ]
})
export class SharedModule { }
