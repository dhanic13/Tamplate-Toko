import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigasiBarComponent } from './navigasi-bar.component';

describe('NavigasiBarComponent', () => {
  let component: NavigasiBarComponent;
  let fixture: ComponentFixture<NavigasiBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavigasiBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigasiBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
