import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { HomeComponent } from './home/home.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';

const routingAplikasi : Routes = [
      {path : '', redirectTo : "home", pathMatch: 'full'},
      {path : 'home', component : HomeComponent},  //, canActivateChild: [AuthGuard]},
      {path : 'product', component : ProductListComponent},
      {path : 'product-detail', component : ProductDetailComponent},
      {path : 'cart', component : ShoppingCartComponent}
];


@NgModule({
      declarations: [
            AppComponent,
            HomeComponent,
            ProductListComponent,
            ShoppingCartComponent,
            ProductDetailComponent
      ],
      imports: [
            BrowserModule, FormsModule, HttpModule,
            RouterModule.forRoot(routingAplikasi),
            SharedModule
      ],
      providers: [],
      bootstrap: [AppComponent]
})
export class AppModule { }
